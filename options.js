/*
 * options.js
 * Copyright (C) 2022 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

function restoreOptions() {
    function setCurrentSearchEngine(result) {
        document.getElementById("searchenginesel").value = result.searchengine || "DuckDuckGo";
        if (result.searchengine == "Custom") {
            document.getElementById("hiddenarea").style.display = "inline";
        }else {
            document.getElementById("hiddenarea").style.display = "none";
        }
      }
    
      function onError(error) {
        console.log(`Error: ${error}`);
      }
    
      var getting = browser.storage.sync.get("searchengine");
      getting.then(setCurrentSearchEngine, onError);

      function setCurrentCustomURL(result) {
        document.getElementById("customurl").value = result.customurl || "";
      }

      var customurlgetting = browser.storage.sync.get("customurl");
      customurlgetting.then(setCurrentCustomURL,onError);

      
}

function choiseChanged() {
    if (document.getElementById("searchenginesel").value == "Custom") {
        document.getElementById("hiddenarea").style.display = "inline";
    } else {
        document.getElementById("hiddenarea").style.display = "none";
    }
    browser.storage.sync.set({
        searchengine: document.getElementById("searchenginesel").value
    });
}

function customURLChanged() {
    browser.storage.sync.set({
        customurl: document.getElementById("customurl").value
    });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("searchenginesel").addEventListener("change",choiseChanged);
document.getElementById("customurl").addEventListener("change",customURLChanged);