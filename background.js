/*
 * background.js
 * Copyright (C) 2022 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

var searchengine ="";
 var searchURL = "";
 var customURL = "";
        
browser.contextMenus.create({
	id: "searchcontextmenu",
	title: "Search web in private mode",
    contexts: ["selection"]
});

browser.contextMenus.onClicked.addListener(async function(info,tab){
    if (info.menuItemId == "searchcontextmenu"){
        //console.log("executed");
       

        var searchquery = info.selectionText;
        searchquery = searchquery.replaceAll("%","%25");
        searchquery = searchquery.replaceAll("+","%2B");
        searchquery = searchquery.replaceAll("!","%21");
        searchquery = searchquery.replaceAll("\"","%22");
        searchquery = searchquery.replaceAll("#","%23");
        searchquery = searchquery.replaceAll("$","%24");
        searchquery = searchquery.replaceAll("&","%26");
        searchquery = searchquery.replaceAll("\'","%27");
        searchquery = searchquery.replaceAll("(","%28");
        searchquery = searchquery.replaceAll(")","%29");
        searchquery = searchquery.replaceAll("*","%2A");
        searchquery = searchquery.replaceAll(",","%2C");
        searchquery = searchquery.replaceAll("-","%2D");
        searchquery = searchquery.replaceAll("/","%2F");
        searchquery = searchquery.replaceAll(":","%3A");
        searchquery = searchquery.replaceAll(";","%3B");
        searchquery = searchquery.replaceAll("<","%3C");
        searchquery = searchquery.replaceAll("=","%3D");
        searchquery = searchquery.replaceAll(">","%3E");
        searchquery = searchquery.replaceAll("?","%3F");
        searchquery = searchquery.replaceAll("@","%40");
        searchquery = searchquery.replaceAll("[","%5B");
        searchquery = searchquery.replaceAll("\\","%5C");
        searchquery = searchquery.replaceAll("]","%5D");
        searchquery = searchquery.replaceAll("^","%5E");
        searchquery = searchquery.replaceAll("_","%5F");
        searchquery = searchquery.replaceAll("`","%60");
        searchquery = searchquery.replaceAll("{","%7B");
        searchquery = searchquery.replaceAll("|","%7C");
        searchquery = searchquery.replaceAll("}","%7D");
        searchquery = searchquery.replaceAll("~","%7E")
        searchquery =  searchquery.replaceAll(" ","+");

        function onError(error) {
            //console.log(`Error: ${error}`);
        }
        function setSearchEngine(result) {
            searchengine = result.searchengine || "DuckDuckGo";
            //console.log("result.searchengine: " + result.searchengine);
            //console.log("searchengine_1: " + searchengine);
            //console.log("customURL_1: ",result.customurl);
        }
        function setCustomURL(result){
            customURL = result.customurl;
            //console.log("searchengine_2: " + searchengine);
            //console.log("searchengine: " + searchengine);

        
        }
        
        var searchenginegetting = browser.storage.sync.get("searchengine");
        await searchenginegetting.then(setSearchEngine, onError);
        //console.log("searchenginegetting.searchengine: " + searchenginegetting.searchengine);
        var sURLgetting = browser.storage.sync.get("customurl");
        await sURLgetting.then(setCustomURL,onError);
        
        switch (searchengine) {
            case "DuckDuckGo":
                searchURL = "https://duckduckgo.com/?q=%s";
                break;
            case "Startpage":
                searchURL = "https://startpage.com/do/search?query=%s";
                break;
            case "Google (Not recommended)":
                searchURL = "https://google.com/search?q=%s";
                break;
            case "Bing (Not recommended)":
                searchURL = "https://bing.com/search?q=%s";
                break;
            case "Custom":
                searchURL = customURL;
                break;
        }
        //console.log("searchquery:" + searchquery);
        
        searchURL = searchURL.replaceAll("%s",searchquery);
        //console.log("searchURL: " + searchURL);

        function decideOpenWindow(result){
            var winlength = result.length;
            for(var i = 0;i <= (winlength-1);i++){
                if (result[i].incognito == true){
                    //console.log("newtab: "+searchURL);
                    browser.tabs.create({windowId: result[i].id,url: searchURL});
                    browser.windows.update(result[i].id,{focused:true});
                    break;
                } else {
                    if (i == (winlength-1)) {
                        //console.log("newWindow: "+searchURL)
                        browser.windows.create({url: searchURL, incognito: true});
                        break;
                    }
                }
            }
        }

        var windowsGettingAll = browser.windows.getAll();
        await windowsGettingAll.then(decideOpenWindow,onError);
        //browser.windows.create({url: searchURL, incognito: true});
    }
})

function onInstalled() {
  //console.log(details.reason);
  browser.tabs.create({
    url: "welcomepage/welcome.html"
  });
}

browser.runtime.onInstalled.addListener(onInstalled);
